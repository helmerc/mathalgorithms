/**
 * A small collection of algorithms implemented in Java.
 */

/**
 * @author Christopher Helmer
 *
 */
public class MathAlgorithms {
	
	public MathAlgorithms() {
		super();
	}
	
	/**
	 * Uses the Euclidian algorithm to find the greatest common divisor of 
	 * the input m and n. Both inputs must be non-negative and non-zero.
	 * @param m is a non-negative, non-zero integer.
	 * @param n is a non-negative, non-zero integer.
	 * @return the greatest common divisor of the inputs m and n.
	 */
	public int GCD(int m, int n) {
		int r = 0;
		if(m <= 0 || n <= 0) {
			return -1;
		}
		while(n != 0) {
			r = m % n;
			m = n;
			n = r;
		}
		return m;
	}
	
	/**
	 * This method determines if the input number is prime or not.
	 * We use the theorem that if n is a composite number, then n has
	 * a prime divisor less than or equal to the square root of n. 
	 * We then know that 2 and 3 are prime, so we check for those. 
	 * Beyond that, we hypothesize that all prime divisors are in the form
	 * 6k +- 1 <= the square root of n.
	 * @param n
	 * @return
	 */
	public boolean isPrime(int n) {
		if(n <= 1) {
			return false;
		} else if( n <= 3) {
			return true;
		}else if(n % 2 == 0 || n % 3 == 0) {
			return false;
		}
		int i = 5;
		while((i * i) < n) {
			if(n % i == 0 || n % (i + 2) == 0) {
				return false;
			} 
			i += 6;
		}
		return true;
	}
	
	/**
	 * Finds the nth Fibonacci number based on the n input.
	 * @param n the input number that is non-negative.
	 * @return the nth Fibonacci number starting with 1.
	 */
	public int fib(int n) {
		int[] fibSeq = new int[n + 1];
		fibSeq[0] = 0;
		fibSeq[1] = 1;
		for(int i = 2; i <= n; i++) {
			fibSeq[i] = fibSeq[i - 1] + fibSeq[i - 2];
		}
		return fibSeq[n];
	}
	
	/**
	 * Recursively finds the nth Fibonacci number.
	 * @param n the input number that is non-negative.
	 * @return the nth Fibonacci number starting with 1.
	 */
	public int fibRecur(int n) {
		//base case
		if(n <= 1) {
			return n;
		} else {
			return fibRecur(n - 1) + fibRecur(n - 2);
		}
	}
	
	/**
	 * Performs basic matrix multiplication on two n x n matrices
	 * without any optimization.
	 * @param A is input matrix
	 * @param B is input matrix
	 * @return the product AB = C
	 */
	public int[][] matrixMultiplication(int[][] A, int[][] B) {
		int length = A.length;
		int[][] C = new int[length][length];
		for(int i = 0; i < length; i++) {
			for(int j = 0; j < length; j++) {
				C[i][j] = 0;
				for(int k = 0; k < length; k++) {
					C[i][j] = C[i][j] + A[i][k] * B[k][j];
				}
			}
		}
		return C;
	}
	
	/**
	 * Calculates the number of binary digits in the binary representation 
	 * of the input number n.
	 * @param n 
	 * @return the number of binary digits needed to represent the input n.
	 */
	public int binary(int n) {
		int count = 1;
		while(n > 1) {
			count += 1;
			n /= 2;
		}
		return count;
	}
	
	/**
	 * Recursively calculates the number of binary digits in the binary 
	 * representation of the input number n.
	 * @param n 
	 * @return the number of binary digits needed to represent the input n.
	 */
	public int binaryRecur(int n) {
		//base case
		if(n == 1) {
			return 1;
		} else {
			return binaryRecur(n / 2) + 1;
		}
	}
	
	/**
	 * Calculates n!.
	 * @param n input number to calculate factorial.
	 * @return n!
	 */
	public int factorial(int n) {
		//base case
		if(n == 0) {
			return 1;
		} else {
			return factorial(n - 1) * n;
		}
	}
	
	/**
	 * Evaluates a polynomial at a given point using Horner's rule.
	 * @param coeff is an array of integers representing the coefficients
	 *        of the polynomial stored from lowest to highest.
	 *        ex: 2x^2 + 3x - 4 would be stored as {-4, 3, 2}
	 * @param x is the numberto evaluate the polynomial at
	 * @return the value of the polynomial at x using Horner's rule.
	 */
	public int horner(int coeff[], int x) {
		int length = coeff.length;
		int total = coeff[length - 1];
		for(int i = length - 2; i >= 0; i--) {
			total = x * total + coeff[i];
		}
		return total;
	}
	
	/**
	 * Calculates a^n using left to right binary exponentiation
	 * where the exponent n is represented by binary digits in an
	 * array.
	 * @param a is an integer 
	 * @param bin is an array of binary digits that represent the exponent n
	 * 		  ordered from lowest to highest.
	 * 		  (you must check to ensure that all digits in the array are 
	 *        binary; 1 or 0)
	 * @return the product a^n
	 */
	public int LRBinExp(int a, int[] bin) {
		int length = bin.length;
		int product = a;
		for(int i = length - 2; i >= 0; i--) {
			product *= product;
			if(bin[i] == 1) {
				product *= a;
			}
		}
		return product;
	}
	
	/**
	 * Calculates a^n using right to left binary exponentiation
	 * where the exponent n is represented by binary digits in an array.
	 * @param a is an integer.
	 * @param bin is an array of binary digits that represent the exponent n
	 * 		  ordered from lowest to highest.
	 * 		  (you must check to ensure that all digits in the array are 
	 *        binary; 1 or 0)
	 * @return the product a^n
	 */
	public int RLBinExp(int a, int[] bin) {
		int length = bin.length;
		int term = a;
		int product = 1;
		if(bin[0] == 1) {
			product = a;
		}
		for(int i = 1; i < length; i++) {
			term *= term;
			if(bin[i] == 1) {
				product *= term;
			}
		}
		return product;
	}

	/**
	 * Find the maximum amount of money that can be collected from
	 * a row of coins without picking any two adjacent coins in the 
	 * row.
	 * @param coins is an array of positive integers indicating coin values.
	 * @return the maximum amount of money that can be collected from 
	 * 		   a row of coins without picking any two adjacent coins.
	 */
	public int CoinRow(int[] coins) {
		int length = coins.length;
		int[] coll = new int[length];
		coll[0] = 0;
		coll[1] = coins[0];
		for(int i = 2; i < length; i++) {
			coll[i] = max(coins[i] + coll[i - 2], coll[i - 1]);
		}
		return coll[length - 1];
	}
	
	private int max(int a, int b) {
		if(a > b) {
			return a;
		} else {
			return b;
		}
	}
	
}
