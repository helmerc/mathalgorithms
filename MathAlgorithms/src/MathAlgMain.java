/**
 * example of how to use the math algorithms in this package.
 */

/**
 * @author Christopher Helmer
 *
 */
public class MathAlgMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MathAlgorithms malg = new MathAlgorithms();
		
		//greatest common divisor checks
		System.out.println("The greatest common divisor of 8 and 36 is: "
						  + malg.GCD(8, 36));
		System.out.println("The greatest common divisor of 200 and 2048 is: "
				  		  + malg.GCD(200, 2048));
		
		System.out.println();
		
		//primality checks
		System.out.println("Is the number 1111 prime?");
		if(malg.isPrime(1111)) 
			System.out.println("yes");
		else
			System.out.println("no");
		
		System.out.println("Is the number 97 prime?");
		if(malg.isPrime(97)) 
			System.out.println("yes");
		else
			System.out.println("no");
		
		System.out.println();
		
		//Fibonacci numbers -- iterative algorithm
		System.out.println("The 13th Fibonacci number is: " + malg.fib(13));
		System.out.println("The 21st Fibonacci number is: " + malg.fib(21));
		System.out.println("The 36th Fibonacci number is: " + malg.fib(36));
		
		System.out.println();
		
		//Fibonacci numbers -- recursive algorithm
		System.out.println("The 13th Fibonacci number is: " + malg.fibRecur(13));
		System.out.println("The 21st Fibonacci number is: " + malg.fibRecur(21));
		System.out.println("The 36th Fibonacci number is: " + malg.fibRecur(36));
		
		System.out.println();
		System.out.println();
		//Matrix multiplication
		int[][] A = new int[][]{{2, 2, 2}, {2, 2, 2}, {2, 2, 2}};
		int[][] B = new int[][]{{2, 2, 2}, {2, 2, 2}, {2, 2, 2}};
		
		int length = A.length;
		int height = A[0].length;
		
		System.out.println("Matrix A: ");
		for(int i = 0; i < length; i++) {
			for(int j = 0; j < height; j++) {
				System.out.print("" + A[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("Matrix B: ");
		for(int i = 0; i < length; i++) {
			for(int j = 0; j < height; j++) {
				System.out.print("" + B[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("A * B: ");
		int C[][] = malg.matrixMultiplication(A, B);
		for(int i = 0; i < length; i++) {
			for(int j = 0; j < height; j++) {
				System.out.print("" + C[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
		
		//Binary digit representation -- iterative algorithm
		System.out.println("Number of binary digits that make up the number 63: " 
				+ malg.binary(63));
		System.out.println("Number of binary digits that make up the number 64: " 
				+ malg.binary(64));
		System.out.println("Number of binary digits that make up the number 127: " 
				+ malg.binary(127));
		System.out.println("Number of binary digits that make up the number 128: " 
				+ malg.binary(128));
		
		System.out.println();
		
		//Binary digit representation -- iterative algorithm
		System.out.println("Number of binary digits that make up the number 63: " 
				+ malg.binaryRecur(63));
		System.out.println("Number of binary digits that make up the number 64: " 
				+ malg.binaryRecur(64));
		System.out.println("Number of binary digits that make up the number 127: " 
				+ malg.binaryRecur(127));
		System.out.println("Number of binary digits that make up the number 128: " 
				+ malg.binaryRecur(128));
		
		System.out.println();
		
		//calculate factorial
		System.out.println("5! = " + malg.factorial(5));
		System.out.println("7! = " + malg.factorial(7));
		System.out.println("10! = " + malg.factorial(10));
		
		System.out.println();
		
		//evaluate polynomials using Horner's rule
		int[] coeff = new int[] {7, 5, -10, -12, -25, 8, 6};
		int x = 2;
		System.out.println("6x^6 + 8x^5 -25x^4 -12x^3 -10x^2 + 5x + 7 = "
				+ malg.horner(coeff, x));
		
		int[] coeff1 = new int[] {7, 5, 10, 2, 5};
		int x1 = 2;
		System.out.println("5x^4 + 2x^3 + 10x^2 + 5x + 7 = "
				+ malg.horner(coeff1, x1));
		
		System.out.println();
		
		//finding a^n using left to right binary exponentiation
		int[] ten = new int[]{0, 1, 0, 1};
		//one should do a check that all numbers are 1's and 0's
		System.out.println("2^10 = " + malg.LRBinExp(2, ten));
		
		int[] twenty = new int[]{0, 0, 1, 0, 1};
		//one should do a check that all numbers are 1's and 0's
		System.out.println("2^20 = " + malg.LRBinExp(2, twenty));
		
		System.out.println();
		
		//finding a^n using right to left binary exponentiation
		//using array from above for ten
		System.out.println("2^10 = " + malg.RLBinExp(2, ten));
		//using array from above for twenty
		System.out.println("2^20 = " + malg.RLBinExp(2, twenty));
		
		System.out.println();
		
		//Using the coin row algorithm, find the largest amount of money 
		//that can be picked up from a row of coins without picking any
		//two adjacent coins. The array represents the row of coins.
		int[] coinRow1 = new int[] {5, 1, 2, 10, 6, 2};
		System.out.print("The largest amount of money from the coin row ");
		System.out.println("5, 1, 2, 10, 6, 2 is ");
		System.out.println("" + + malg.CoinRow(coinRow1));
		
		int[] coinRow2 = new int[] {25, 2, 1, 50, 10, 5, 25};
		System.out.print("The largest amount of money from the coin row ");
		System.out.println("25, 2, 1, 50, 10, 5, 25 is ");
		System.out.println("" + + malg.CoinRow(coinRow2));

	}

}
