# README #

### What is this repository for? ###

* A small library of math algorithms for my own use/practice, implemented in Java.

### How do I get set up? ###

* MathAlgMain.java gives examples of how to use each algorithm and displays results in the console.

### Author ###

* Christopher Helmer

## Resources ##

* "Introduction to the Design and Analysis of Algorithms" - Anany Levitin
* https://en.wikipedia.org/wiki/Primality_test

### Who do I talk to? ###

* Christopher Helmer
* christopher.helmer@yahoo.com